﻿namespace UltimatumGame.Service.Contract
{
    public class Error
    {
        public string ID { get; set; }

        public string Message { get; set; }
    }
}
