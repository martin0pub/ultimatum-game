﻿namespace UltimatumGame.Service.Contract.Game.Respond
{
    public class Response
    {
        public Digest.Model Game { get; set; }
    }
}
