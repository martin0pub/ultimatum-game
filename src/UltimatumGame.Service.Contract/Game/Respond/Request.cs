﻿namespace UltimatumGame.Service.Contract.Game.Respond
{
    public class Request
    {
        public long PlayerID { get; set; }

        public string GameID { get; set; }

        public int ResponseID { get; set; }
    }
}
