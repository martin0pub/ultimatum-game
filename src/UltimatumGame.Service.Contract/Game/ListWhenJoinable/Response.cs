﻿namespace UltimatumGame.Service.Contract.Game.ListWhenJoinable
{
    using System.Collections.Generic;

    public class Response
    {
        public IEnumerable<Digest.Model> Games { get; set; }
    }
}
