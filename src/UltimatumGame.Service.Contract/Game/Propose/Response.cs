﻿namespace UltimatumGame.Service.Contract.Game.Propose
{
    public class Response
    {
        public Digest.Model Game { get; set; }
    }
}
