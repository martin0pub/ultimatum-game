﻿namespace UltimatumGame.Service.Contract.Game.Propose
{
    public class Request
    {
        public long PlayerID { get; set; }

        public string GameID { get; set; }

        public int ValueForProposer { get; set; }

        public int ValueForResponder { get; set; }
    }
}
