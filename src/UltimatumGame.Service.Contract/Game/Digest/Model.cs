﻿namespace UltimatumGame.Service.Contract.Game.Digest
{
    public class Model
    {
        public string GameID { get; set; }

        public int StateID { get; set; }
    }
}
