﻿namespace UltimatumGame.Service.Contract.Game.ReportList
{
    using System.Collections.Generic;

    public class Response
    {
        public IEnumerable<Report.Model> Games { get; set; }
    }
}
