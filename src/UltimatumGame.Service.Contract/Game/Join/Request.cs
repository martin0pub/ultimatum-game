﻿namespace UltimatumGame.Service.Contract.Game.Join
{
    public class Request
    {
        public long PlayerID { get; set; }

        public string GameID { get; set; }
    }
}
