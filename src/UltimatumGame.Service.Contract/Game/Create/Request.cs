﻿namespace UltimatumGame.Service.Contract.Game.Create
{
    public class Request
    {
        public long PlayerID { get; set; }

        public int PlayerRoleID { get; set; }
    }
}
