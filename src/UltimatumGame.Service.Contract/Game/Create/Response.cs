﻿namespace UltimatumGame.Service.Contract.Game.Create
{
    public class Response
    {
        public Digest.Model Game { get; set; }
    }
}
