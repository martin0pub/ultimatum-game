﻿namespace UltimatumGame.Service.Contract
{
    public class ApiResponse<T> : ApiResponse
    {
        public new T Data { get; set; }

        public ApiResponse()
        {
        }

        public ApiResponse(T data)
        {
            Data = data;
        }
    }
}
