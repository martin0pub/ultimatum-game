﻿namespace UltimatumGame.Service.Contract
{
    using System.Collections.Generic;

    public class ApiResponse
    {
        public object Data { get; set; }

        public IEnumerable<Error> Errors { get; set; }

        public object Meta { get; set; }

        public ApiResponse()
        {
        }

        public ApiResponse(object data)
        {
            Data = data;
        }
    }
}
