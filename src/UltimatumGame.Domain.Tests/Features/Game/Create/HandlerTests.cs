﻿namespace UltimatumGame.Domain.Tests.Features.Game.Create
{
    using FluentAssertions;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Features.Game.Create;
    using UltimatumGame.Domain.Glossary;
    using Xunit;
    using Xunit.Abstractions;

    public class HandlerTests : IDisposable
    {
        private readonly ITestOutputHelper _output;
        private readonly DbConnection _dbConnection;
        private readonly DbContextOptions<GameContext> _dbContextOptions;
        private readonly GameContext _dbContext;

        public HandlerTests(ITestOutputHelper output)
        {
            _output = output;

            // NOTE: In-memory database only exists while the connection is open.
            //_dbConnection = new SqliteConnection("DataSource=:memory:");
            _dbConnection = new SqliteConnection("DataSource=games.db");
            _dbConnection.Open();

            _dbContextOptions = new DbContextOptionsBuilder<GameContext>()
                .UseSqlite(_dbConnection)
                .Options;

            // TODO: FIXME: Make this unnecessary.
            // Create the schema in the database.
            using (var context = new GameContext(_dbContextOptions))
            {
                context.Database.EnsureCreated();
            }

            _dbContext = new GameContext(_dbContextOptions);
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
            _dbConnection?.Close();
            _dbConnection?.Dispose();
        }

        public static IEnumerable<object[]> ResultStateData =>
            new List<object[]>
            {
                 new object[] { PlayerRole.Proposer.ID, GameState.WaitingForResponder.ID },
                 new object[] { PlayerRole.Responder.ID, GameState.WaitingForProposer.ID },
            };

        [Theory]
        [MemberData(nameof(ResultStateData))]
        public async Task Create_ValidInput_CreatedGameHasValidState(int creatorRoleID, int postCreateGameStateID)
        {
            var command = new Command
            {
                PlayerID = 1234, // NOTE: Not random enough.
                PlayerRoleID = creatorRoleID,
            }; // TODO: FIXME: Build proper input.
            var ct = new CancellationToken();
            var handler = new Handler(_dbContext);

            var result = await handler.Handle(command, ct);

            result.Should().NotBeNull();
            result.GameID.Should().NotBeNullOrEmpty();
            result.StateID.Should().Be(postCreateGameStateID);
        }

        [Fact(Skip = "for manual run only")]
        public async Task Experiment1()
        {
            // Run the test against one instance of the context.
            using (var context = new GameContext(_dbContextOptions))
            {
                var gameID = Guid.NewGuid();
                var playerID = 1234;
                _output.WriteLine("game: {0}; player: {1}", gameID.ToString("N"), playerID.ToString("N"));
                context.Games.Add(new Game
                {
                    GameID = gameID,
                    ProposerID = playerID,
                    StateID = GameState.WaitingForResponder.ID
                });

                gameID = Guid.NewGuid();
                playerID = 1234;
                _output.WriteLine("game: {0}; player: {1}", gameID.ToString("N"), playerID.ToString("N"));
                context.Games.Add(new Game
                {
                    GameID = gameID,
                    ResponderID = playerID,
                    StateID = GameState.WaitingForProposer.ID
                });

                context.SaveChanges();
            }

            // Use a separate instance of the context to verify correct data was saved to database.
            using (var context = new GameContext(_dbContextOptions))
            {
                _output.WriteLine("----");
                foreach (var game in context.Games.AsNoTracking().ToList())
                {
                    _output.WriteLine(
                        "game: {0}; state: {1}; proposer: {2}; responder: {3}",
                        game.GameID.ToString("N"),
                        game.StateID,
                        game.ProposerID?.ToString("N"),
                        game.ResponderID?.ToString("N"));
                }
            }
        }

        [Fact(Skip = "for manual run only")]
        public async Task Experiment2()
        {
            // NOTE: In-memory database only exists while the connection is open.
            //var connection = new SqliteConnection("DataSource=:memory:");
            var connection = new SqliteConnection("DataSource=games.db");
            connection.Open();

            try
            {
                var options = new DbContextOptionsBuilder<GameContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database.
                //using (var context = new GameContext(options))
                //{
                //    context.Database.EnsureCreated();
                //}

                // Run the test against one instance of the context.
                using (var context = new GameContext(options))
                {
                    var gameID = Guid.NewGuid();
                    var playerID = 1234;
                    _output.WriteLine("game: {0}; player: {1}", gameID.ToString("N"), playerID.ToString("N"));
                    context.Games.Add(new Game
                    {
                        GameID = gameID,
                        ProposerID = playerID,
                        StateID = GameState.WaitingForResponder.ID
                    });

                    gameID = Guid.NewGuid();
                    playerID = 1234;
                    _output.WriteLine("game: {0}; player: {1}", gameID.ToString("N"), playerID.ToString("N"));
                    context.Games.Add(new Game
                    {
                        GameID = gameID,
                        ResponderID = playerID,
                        StateID = GameState.WaitingForProposer.ID
                    });

                    context.SaveChanges();
                }

                // Use a separate instance of the context to verify correct data was saved to database
                using (var context = new GameContext(options))
                {
                    _output.WriteLine("----");
                    foreach (var game in context.Games.AsNoTracking().ToList())
                    {
                        _output.WriteLine(
                            "game: {0}; state: {1}; proposer: {2}; responder: {3}",
                            game.GameID.ToString("N"),
                            game.StateID,
                            game.ProposerID?.ToString("N"),
                            game.ResponderID?.ToString("N"));
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            //connection = new SqliteConnection("DataSource=:memory:");
            connection = new SqliteConnection("DataSource=games.db");
            connection.Open();
            try
            {
                var options = new DbContextOptionsBuilder<GameContext>()
                    .UseSqlite(connection)
                    .Options;

                // Create the schema in the database.
                using (var context = new GameContext(options))
                {
                    context.Database.EnsureCreated();
                }

                // Use a separate instance of the context to verify correct data was saved to database.
                using (var context = new GameContext(options))
                {
                    _output.WriteLine("----");
                    foreach (var game in context.Games.AsNoTracking().ToList())
                    {
                        _output.WriteLine(
                            "game: {0}; state: {1}; proposer: {2}; responder: {3}",
                            game.GameID.ToString("N"),
                            game.StateID,
                            game.ProposerID?.ToString("N"),
                            game.ResponderID?.ToString("N"));
                    }
                }
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
