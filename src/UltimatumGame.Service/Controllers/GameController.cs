﻿namespace UltimatumGame.Service.Controllers
{
    using AutoMapper;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Features;
    using UltimatumGame.Domain.Glossary;
    using UltimatumGame.Service.Contract;

    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly ILogger<GameController> _logger;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GameController(
            ILogger<GameController> logger,
            IMapper mapper,
            IMediator mediator)
        {
            _logger = logger;
            _mapper = mapper;
            _mediator = mediator;
        }

        // API 1: Create a game
        // POST api/game
        [HttpPost]
        public async Task<ApiResponse<Contract.Game.Create.Response>> Post(
            [FromBody] Contract.Game.Create.Request input)
        {
            // TODO: Validate that 'input' is not null.

            var game = await _mediator.Send(
                new UltimatumGame.Domain.Features.Game.Create.Command
                {
                    PlayerID = input.PlayerID,
                    PlayerRoleID = input.PlayerRoleID,
                });

            var response = new ApiResponse<Contract.Game.Create.Response>(
                new Contract.Game.Create.Response
                {
                    Game = _mapper.Map<Contract.Game.Digest.Model>(game)
                });
#if DEBUG
            response.Meta = new { Body = input };
#endif

            return response;
        }

        // API 2: Join a game
        // PUT api/game/55555555555555555555555555555555
        [HttpPut("{id}")]
        public async Task<ApiResponse<Contract.Game.Join.Response>> Put(
            string id,
            [FromBody] Contract.Game.Join.Request input)
        {
            var game = await _mediator.Send(
                new UltimatumGame.Domain.Features.Game.Join.Command
                {
                    PlayerID = input.PlayerID,
                    GameID = id,
                });

            var response = new ApiResponse<Contract.Game.Join.Response>(
                new Contract.Game.Join.Response
                {
                    Game = _mapper.Map<Contract.Game.Digest.Model>(game)
                });
#if DEBUG
            response.Meta = new { Url = new { Id = id }, Body = input };
#endif

            return response;
        }

        // API 3: Propose a split
        // PUT api/game/55555555555555555555555555555555/proposal
        [HttpPost("{id}/proposal")]
        public async Task<ApiResponse<Contract.Game.Propose.Response>> Post(
            string id,
            [FromBody] Contract.Game.Propose.Request input)
        {
            var game = await _mediator.Send(
                new UltimatumGame.Domain.Features.Game.Propose.Command
                {
                    GameID = id,
                    PlayerID = (input?.PlayerID).GetValueOrDefault(),
                    ValueForProposer = (input?.ValueForProposer).GetValueOrDefault(),
                    ValueForResponder = (input?.ValueForResponder).GetValueOrDefault(),
                });

            var response = new ApiResponse<Contract.Game.Propose.Response>(
                new Contract.Game.Propose.Response
                {
                    Game = _mapper.Map<Contract.Game.Digest.Model>(game)
                });
#if DEBUG
            response.Meta = new { Url = new { Id = id }, Body = input };
#endif

            return response;
        }

        // API 4: Respond to split
        // PUT api/game/55555555555555555555555555555555/proposal
        [HttpPut("{id}/proposal")]
        public async Task<ApiResponse<Contract.Game.Respond.Response>> Put(
            string id,
            [FromBody] Contract.Game.Respond.Request input)
        {
            var game = await _mediator.Send(
                new UltimatumGame.Domain.Features.Game.Respond.Command
                {
                    GameID = id,
                    PlayerID = (input?.PlayerID).GetValueOrDefault(),
                    ResponseID = (input?.ResponseID).GetValueOrDefault(),
                });

            var response = new ApiResponse<Contract.Game.Respond.Response>(
                new Contract.Game.Respond.Response
                {
                    Game = _mapper.Map<Contract.Game.Digest.Model>(game)
                });
#if DEBUG
            response.Meta = new { Url = new { Id = id }, Body = input };
#endif

            return response;
        }

        // API 6: Get game data
        // GET api/game/55555555555555555555555555555555
        [HttpGet("{id}")]
        public async Task <ApiResponse<Contract.Game.Report.Response>> Get(string id)
        {
            var game = await _mediator.Send(
                new UltimatumGame.Domain.Features.Game.Report.Query
                {
                    GameID = id
                });

            var response = new ApiResponse<Contract.Game.Report.Response>(
                new Contract.Game.Report.Response
                {
                    Game = _mapper.Map<Contract.Game.Report.Model>(game)
                });
#if DEBUG
            response.Meta = new { Url = new { Id = id } };
#endif

            return response;
        }
    }
}
