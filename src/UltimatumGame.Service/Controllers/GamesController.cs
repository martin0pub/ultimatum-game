﻿namespace UltimatumGame.Service.Controllers
{
    using AutoMapper;
    using MediatR;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Logging;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using UltimatumGame.Service.Contract;

    [Route("api/[controller]")]
    [ApiController]
    public class GamesController : ControllerBase
    {
        private readonly ILogger<GameController> _logger;
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public GamesController(
            ILogger<GameController> logger,
            IMapper mapper,
            IMediator mediator)
        {
            _logger = logger;
            _mapper = mapper;
            _mediator = mediator;
        }

        // API 7: List all games (in progress?)
        // GET api/games
        [HttpGet]
        public async Task<ApiResponse<Contract.Game.ReportList.Response>> Get()
        {
            var games = await _mediator.Send(
                new Domain.Features.Game.ReportList.Query());

            var response = new ApiResponse<Contract.Game.ReportList.Response>(
                new Contract.Game.ReportList.Response
                {
                    Games = _mapper.Map<IEnumerable<Contract.Game.Report.Model>>(games)
                });

            return response;
        }

        // API 8: List games to join
        // GET api/games/to_join
        [HttpGet("to_join")]
        public async Task<ApiResponse<Contract.Game.ListWhenJoinable.Response>> Get(
            long playerID)
            //Contract.Game.ListWhenJoinable.Request input)
        {
            var games = await _mediator.Send(
                new Domain.Features.Game.DigestListWhenJoinable.Query
                {
                    //PlayerID = input.PlayerID
                    PlayerID = playerID
                });

            var response = new ApiResponse<Contract.Game.ListWhenJoinable.Response>(
                new Contract.Game.ListWhenJoinable.Response
                {
                    Games = _mapper.Map<IEnumerable<Contract.Game.Digest.Model>>(games)
                });
#if DEBUG
            //response.Meta = new { Body = input };
#endif

            return response;
        }
    }
}
