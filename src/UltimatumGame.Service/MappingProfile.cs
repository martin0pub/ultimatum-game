﻿namespace UltimatumGame.Service
{
    using AutoMapper;

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Domain.Models.Game.Digest.Model, Contract.Game.Digest.Model>();

            CreateMap<Domain.Models.Game.Report.Model, Contract.Game.Report.Model>()
                .ForMember(dst => dst.ResponseID, opt => opt.MapFrom(src => src.DecisionID));
        }
    }
}
