﻿namespace UltimatumGame.Service
{
    using AutoMapper;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using MediatR;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.HttpsPolicy;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Data.Sqlite;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using UltimatumGame.Domain.Data;
    using System.IO;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var dataPath = Path.Combine(AppContext.BaseDirectory, "App_Data");
            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }
            var datafilePath = Path.Combine(dataPath, "games.db");

            services.AddDbContext<GameContext>(options =>
                options.UseSqlite($"DataSource={datafilePath}")); // NOTE: Hard-coded DB reference.

            services.AddAutoMapper(typeof(Startup));

            // NOTE: The type is not important as it's used as merely a reference to the assembly it is in.
            services.AddMediatR(typeof(UltimatumGame.Domain.Features.Game.Create.Command));
            //services.AddScoped(
            //    typeof(IPipelineBehavior<,>));

            services.AddCors();

            services
                .AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(_ =>
                    _.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Disable CORS to make the distributed development easier.
            app.UseCors(policy => policy
                .AllowAnyOrigin()
                .WithMethods("GET", "POST", "PUT", "DELETE")
                .WithHeaders("Content-Type")
                .Build());

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
