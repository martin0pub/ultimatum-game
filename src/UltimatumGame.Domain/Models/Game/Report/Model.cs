﻿namespace UltimatumGame.Domain.Models.Game.Report
{
    public class Model
    {
        public string GameID { get; set; }

        public int StateID { get; set; }

        public long? ProposerID { get; set; }

        public long? ResponderID { get; set; }

        public int? ValueForProposer { get; set; }

        public int? ValueForResponder { get; set; }

        public int? DecisionID { get; set; }
    }
}
