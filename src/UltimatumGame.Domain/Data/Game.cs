﻿namespace UltimatumGame.Domain.Data
{
    using System;

    public class Game
    {
        public Guid GameID { get; set; }

        public int StateID { get; set; }

        //[ConcurrencyCheck]
        public DateTimeOffset StateTimestamp { get; set; }

        public long? ProposerID { get; set; }

        public long? ResponderID { get; set; }

        public int? ValueForProposer { get; set; }

        public int? ValueForResponder { get; set; }

        public int? DecisionID { get; set; }

        //[Timestamp]
        public byte[] Timestamp { get; set; }
    }
}
