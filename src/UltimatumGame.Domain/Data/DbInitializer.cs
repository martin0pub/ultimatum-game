﻿namespace UltimatumGame.Domain.Data
{
    public static class DbInitializer
    {
        public static void Initialize(GameContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
