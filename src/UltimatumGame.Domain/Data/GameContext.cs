﻿namespace UltimatumGame.Domain.Data
{
    using Microsoft.EntityFrameworkCore;

    public class GameContext : DbContext
    {
        public GameContext(DbContextOptions<GameContext> options)
            : base(options)
        {
        }

        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Game>()
                .Property(p => p.StateTimestamp)
                .IsConcurrencyToken();
            modelBuilder.Entity<Game>()
                .Property(p => p.Timestamp)
                .IsRowVersion();
        }
    }
}
