﻿namespace UltimatumGame.Domain.Features.Game.Respond
{
    using MediatR;

    public class Command : IRequest<Models.Game.Digest.Model>
    {
        public long PlayerID { get; set; }

        public string GameID { get; set; }

        public int ResponseID { get; set; }
    }
}
