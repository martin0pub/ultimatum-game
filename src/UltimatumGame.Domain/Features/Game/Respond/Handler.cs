﻿namespace UltimatumGame.Domain.Features.Game.Respond
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Glossary;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Command, Game.Digest.Model>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Game.Digest.Model> Handle(Command command, CancellationToken cancellationToken)
        {
            var game = _dbContext.Games.Single(o => o.GameID.ToString("N") == command.GameID);

            // TODO: Prevent operation if the state is not WaitingForResponderAction.
            // TODO: Prevent operation if player is not responder.
            // TODO: Prevent operation if the response value is not valid.

            game.DecisionID = command.ResponseID;
            game.StateID = GameState.Completed.ID;

            await _dbContext.SaveChangesAsync();

            if (command.ResponseID == ResponderDecision.Accept.ID)
            {
                // TODO: Build notification for score service.
            }
            else if (command.ResponseID == ResponderDecision.Reject.ID)
            {
                // TODO: Build notification for score service.
            }
            // TODO: Notify score service.

            var list = await _dbContext.Games
                .Where(o => o.GameID.ToString("N") == command.GameID)
                .Select(o =>
                    new Game.Digest.Model
                    {
                        GameID = o.GameID.ToString("N"),
                        StateID = o.StateID
                    })
                .ToListAsync();

            return list.Single();
        }
    }
}
