﻿namespace UltimatumGame.Domain.Features.Game.Create
{
    using MediatR;
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Glossary;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Command, Game.Digest.Model>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Game.Digest.Model> Handle(Command command, CancellationToken cancellationToken)
        {
            var gameState = command.PlayerRoleID == PlayerRole.Proposer.ID
                ? GameState.WaitingForResponder.ID
                // => command.UserRoleID == PlayerRole.Responder.ID
                : GameState.WaitingForProposer.ID;

            var game = new Data.Game
            {
                GameID = Guid.NewGuid(),
                StateID = gameState,
            };

            if (command.PlayerRoleID == PlayerRole.Proposer.ID)
            {
                game.ProposerID = command.PlayerID;
            }
            else // => command.UserRoleID == PlayerRole.Responder.ID
            {
                game.ResponderID = command.PlayerID;
            }

            await _dbContext.Games.AddAsync(game);
            await _dbContext.SaveChangesAsync();

            var model = new Game.Digest.Model
            {
                GameID = game.GameID.ToString("N"),
                StateID = game.StateID,
            };

            return model;
        }
    }
}
