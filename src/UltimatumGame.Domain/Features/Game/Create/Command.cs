﻿namespace UltimatumGame.Domain.Features.Game.Create
{
    using MediatR;

    public class Command : IRequest<Models.Game.Digest.Model>
    {
        public long PlayerID { get; set; }

        public int PlayerRoleID { get; set; }
    }
}
