﻿namespace UltimatumGame.Domain.Features.Game.DigestListWhenJoinable
{
    using MediatR;
    using System.Collections.Generic;

    public class Query : IRequest<IEnumerable<Models.Game.Digest.Model>>
    {
        public long PlayerID { get; set; }
    }
}
