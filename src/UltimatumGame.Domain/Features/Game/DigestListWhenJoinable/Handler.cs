﻿namespace UltimatumGame.Domain.Features.Game.DigestListWhenJoinable
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Glossary;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Query, IEnumerable<Game.Digest.Model>>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<IEnumerable<Game.Digest.Model>> Handle(Query query, CancellationToken cancellationToken)
        {
            var games = await _dbContext.Games
                .Where(o =>
                    (   o.StateID == GameState.WaitingForProposer.ID
                        || o.StateID == GameState.WaitingForResponder.ID)
                    && o.ProposerID != query.PlayerID
                    && o.ResponderID != query.PlayerID)
                .Select(o =>
                    new Game.Digest.Model
                    {
                        GameID = o.GameID.ToString("N"),
                        StateID = o.StateID,
                    })
                .ToListAsync();

            return games;
        }
    }
}
