﻿namespace UltimatumGame.Domain.Features.Game.Join
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Glossary;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Command, Game.Digest.Model>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Game.Digest.Model> Handle(Command command, CancellationToken cancellationToken)
        {
            var game = _dbContext.Games.Single(o => o.GameID.ToString("N") == command.GameID);
            if (game.ProposerID == null)
            {
                game.ProposerID = command.PlayerID;
            }
            else if (game.ResponderID == null)
            {
                game.ResponderID = command.PlayerID;
            }
            else
            {
                throw new Exception("Cannot join game. Game is full.");
            }
            game.StateID = GameState.WaitingForProposerAction.ID;

            await _dbContext.SaveChangesAsync();

            var list = await _dbContext.Games
                .Where(o => o.GameID.ToString("N") == command.GameID)
                .Select(o =>
                    new Game.Digest.Model
                    {
                        GameID = o.GameID.ToString("N"),
                        StateID = o.StateID
                    })
                .ToListAsync();

            return list.Single();
        }
    }
}
