﻿namespace UltimatumGame.Domain.Features.Game.Leave
{
    public class Command
    {
        public long PlayerID { get; set; }

        public int PlayerRoleID { get; set; }

        public string GameID { get; set; }
    }
}
