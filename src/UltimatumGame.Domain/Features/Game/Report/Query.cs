﻿namespace UltimatumGame.Domain.Features.Game.Report
{
    using MediatR;

    public class Query : IRequest<Models.Game.Report.Model>
    {
        public string GameID { get; set; }
    }
}
