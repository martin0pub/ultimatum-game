﻿namespace UltimatumGame.Domain.Features.Game.Report
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Query, Game.Report.Model>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Game.Report.Model> Handle(Query query, CancellationToken cancellationToken)
        {
            var games = await _dbContext.Games
                .Where(o => o.GameID.ToString("N") == query.GameID)
                .Select(o =>
                    new Game.Report.Model
                    {
                        GameID = o.GameID.ToString("N"),
                        StateID = o.StateID,
                        ProposerID = o.ProposerID,
                        ResponderID = o.ResponderID,
                        ValueForProposer = o.ValueForProposer,
                        ValueForResponder = o.ValueForResponder,
                        DecisionID = o.DecisionID
                    })
                .ToListAsync();

            return games.Single();
        }
    }
}
