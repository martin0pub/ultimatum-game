﻿namespace UltimatumGame.Domain.Features.Game.ReportList
{
    using MediatR;
    using System.Collections.Generic;

    public class Query : IRequest<IEnumerable<Models.Game.Report.Model>>
    {
    }
}
