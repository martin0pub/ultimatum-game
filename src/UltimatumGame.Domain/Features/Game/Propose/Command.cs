﻿namespace UltimatumGame.Domain.Features.Game.Propose
{
    using MediatR;

    public class Command : IRequest<Models.Game.Digest.Model>
    {
        public long PlayerID { get; set; }

        public string GameID { get; set; }

        public int ValueForProposer { get; set; }

        public int ValueForResponder { get; set; }
    }
}
