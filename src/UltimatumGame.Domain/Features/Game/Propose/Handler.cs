﻿namespace UltimatumGame.Domain.Features.Game.Propose
{
    using MediatR;
    using Microsoft.EntityFrameworkCore;
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UltimatumGame.Domain.Data;
    using UltimatumGame.Domain.Glossary;
    using Game = UltimatumGame.Domain.Models.Game;

    public class Handler : IRequestHandler<Command, Game.Digest.Model>
    {
        private readonly GameContext _dbContext;

        public Handler(GameContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Game.Digest.Model> Handle(Command command, CancellationToken cancellationToken)
        {
            var game = _dbContext.Games.Single(o => o.GameID.ToString("N") == command.GameID);

            // TODO: Prevent operation if the state is not WaitingForProposerAction.
            // TODO: Prevent operation if player is not proposer.
            // TODO: Prevent operation if values are not valid, i.e. negative or their sum is beyond the limit.

            game.StateID = GameState.WaitingForResponderAction.ID;
            game.ValueForProposer = command.ValueForProposer;
            game.ValueForResponder = command.ValueForResponder;

            await _dbContext.SaveChangesAsync();

            var list = await _dbContext.Games
                .Where(o => o.GameID.ToString("N") == command.GameID)
                .Select(o =>
                    new Game.Digest.Model
                    {
                        GameID = o.GameID.ToString("N"),
                        StateID = o.StateID
                    })
                .ToListAsync();

            return list.Single();
        }
    }
}
