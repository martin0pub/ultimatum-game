﻿namespace UltimatumGame.Domain.Glossary
{
    public class PlayerRole
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int TurnOrder { get; set; } // TODO: Figure out whether this is necessary.

        public static PlayerRole Proposer = new PlayerRole
        {
            ID = 1,
            Name = "proposer",
            TurnOrder = 1,
        };

        public static PlayerRole Responder = new PlayerRole
        {
            ID = 2,
            Name = "responder",
            TurnOrder = 2,
        };
    }
}
