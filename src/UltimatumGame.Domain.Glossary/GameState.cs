﻿namespace UltimatumGame.Domain.Glossary
{
    public class GameState
    {
        public int ID { get; set; }

        public string Name { get; set; }

        //public static GameState Void = new GameState
        //{
        //    ID = 0,
        //    Name = "void",
        //};

        public static GameState Empty = new GameState
        {
            ID = 1,
            Name = "empty", // waiting for players
        };

        public static GameState WaitingForResponder = new GameState
        {
            ID = 2,
            Name = "waiting for responder",
        };

        public static GameState WaitingForProposer = new GameState
        {
            ID = 3,
            Name = "waiting for proposer",
        };

        public static GameState WaitingForProposerAction = new GameState
        {
            ID = 4,
            Name = "waiting for proposer action",
        };

        public static GameState WaitingForResponderAction = new GameState
        {
            ID = 5,
            Name = "waiting for responder action",
        };

        public static GameState Completed = new GameState
        {
            ID = 6,
            Name = "completed",
        };

        public static GameState Disposable = new GameState
        {
            ID = 7,
            Name = "void",
        };
    }
}
