﻿namespace UltimatumGame.Domain.Glossary
{
    public class ResponderDecision
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public static ResponderDecision Accept = new ResponderDecision
        {
            ID = 1,
            Name = "accept",
        };

        public static ResponderDecision Reject = new ResponderDecision
        {
            ID = 2,
            Name = "reject",
        };
    }
}
