=================================
Ultimatum game - Game service/API
=================================

- Endpoints

  - Overview

  - Details

- Values

  - Player role

  - Responder decision

  - Game state

Endpoints
=========

Overview
--------

* Create a game (API 1)::

    POST /api/game

* Join a game (API 2)::

    PUT /api/game/GAME_ID

* Leave a game (API 3)::
  
    DELETE /api/game/GAME_ID

  *NOTE:* This will be implemented later.

* Propose a split (API 4)::

    POST /api/game/GAME_ID/proposal

* Respond to a split (API 5)::

    PUT  /api/game/GAME_ID/proposal

* Get game data (API 6)::

    GET /api/game/GAME_ID

* List all games in progress (API 7)::

    GET /api/games

  *NOTE:* This will be implemented later.

* List games to join (API 8)::

    GET /api/games/to_join

* List games to rejoin (API 9)::

    GET /api/games/to_rejoin

  *NOTE:* This will be implemented later.

Details
-------

*NOTE:* All responses are wrapped in an API response envelope with the
following structure::

    {
        "data": ENDPOINT_RESULT,
        "errors": ARRAY_OF_ERRORS_OR_NULL
    }

A response should be interpreted as follows:

#.  If the ``errors`` element is present, is not null, and is an array of at
    least one element, then processing the request encountered an error and
    was not successful.

    In that case the ``errors`` element is an array of ``Error`` objects. Each
    ``Error`` object has the following structure::

        {
            "id": STRING_VALUE,
            "message": STRING_VALUE
        }

#.  If the ``errors`` element is not present, is null, or is an empty array,
    then processing the request was successful.

    In that case the ``data`` element holds ``ENDPOINT_RESULT`` - the actual
    result of the API endpoint (as described below).

Create a game (API 1)
~~~~~~~~~~~~~~~~~~~~~

Input
+++++

* playerID: An integer identifying the player.
* playerRoleID: An integer value for `Player role`.

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.

Example request
+++++++++++++++

::

    POST /api/game

    {
        "playerID": 1234,
        "playerRoleID": 1
    }

Example response
++++++++++++++++

::

    {
        "game": {
            "gameID": "55555555555555555555555555555555",
            "stateID": 2
        }
    }

Join a game (API 2)
~~~~~~~~~~~~~~~~~~~

Input
+++++

* playerID: An integer identifying the player.

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.

Example request
+++++++++++++++

::

    PUT /game/GAME_ID

    {
        "playerID": 5678
    }

Example response
++++++++++++++++

::

    {
        "game": {
            "gameID": "55555555555555555555555555555555",
            "stateID": 4
        }
    }

Propose a split (API 4)
~~~~~~~~~~~~~~~~~~~~~~~

Input
+++++

* gameID: A string identifying the game.
* playerID: An integer identifying the player.
* valueForProposer: An integer value of the proposed amount to be given to the proposer.
* valueForResponder: An integer value of the proposed amount to be given to the responder.

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.

Example request
+++++++++++++++

::

    POST /api/game/GAME_ID/proposal

    {
        "gameID": "55555555555555555555555555555555",
        "playerID": 1234,
        "valueForProposer": 49,
        "valueForResponder": 51
    }

Example response
++++++++++++++++

::

    {
        "game": {
            "gameID": "55555555555555555555555555555555",
            "stateID": 5
        }
    }

Respond to a split (API 5)
~~~~~~~~~~~~~~~~~~~~~~~~~~

Input
+++++

* gameID: A string identifying the game.
* playerID: An integer identifying the player.
* responseID: An integer value for the responder decision (see `Responder decision`).

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.

Example request
+++++++++++++++

::

    PUT  /api/game/GAME_ID/proposal

    {
        "gameID": "55555555555555555555555555555555",
        "playerID": 5678,
        "responseID": 1
    }

Example response
++++++++++++++++

::

    {
        "game": {
            "gameID": "55555555555555555555555555555555",
            "stateID": 6
        }
    }

Get game data (API 6)
~~~~~~~~~~~~~~~~~~~~~

Input
+++++

* gameID: A string identifying the game.
* playerID: An integer identifying the player.

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.
* valueForProposer: An integer value of the proposed amount to be given to the proposer.
* valueForResponder: An integer value of the proposed amount to be given to the responder.
* responseID: An integer value for the responder decision (see `Responder decision`).

Example request
+++++++++++++++

::

    GET /api/game/GAME_ID

    {
        "gameID": "55555555555555555555555555555555",
        "playerID": 1234
    }

Example response
++++++++++++++++

::

    {
        "game": {
            "gameID": "55555555555555555555555555555555",
            "stateID": 6,
            "valueForProposer": 49,
            "valueForResponder": 51
            "responseID": 1
        }
    }

List games to join (API 8)
~~~~~~~~~~~~~~~~~~~~~~~~~~

Input
+++++

* playerID: An integer identifying the player.

Output
++++++

* gameID: A string identifying the game.
* stateID: An integer value for `Game state`.

Example request
+++++++++++++++

::

    GET /api/games/to_join

    {
        "playerID": 5678
    }

Example response
++++++++++++++++

::

    {
        "games": [
            {
                "gameID": "66666666666666666666666666666666",
                "stateID": 2
            },
            {
                "gameID": "77777777777777777777777777777777",
                "stateID": 3
            }
        ]
    }

Values
======

Player role
-----------

``Player role`` is represented as an integer. The valid roles have the following values:

* **1**: proposer
* **2**: responder

Responder decision
------------------

``Responder decision`` is represented as an integer. The valid decisions have the following values:

* **1**: accept
* **2**: reject

Game state
----------

``Responder decision`` is represented as an integer. The valid states have the following values:

* **2**: waiting for responder to join
* **3**: waiting for proposer to join
* **4**: waiting for responder to act
* **5**: waiting for proposer to act
* **6**: completed

.. vim:ft=rst: